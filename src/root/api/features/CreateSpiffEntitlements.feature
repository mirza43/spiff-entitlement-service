Feature: Create Spiff Entitlements
  Creates spiff entitlemens

  Background:
    Given a spiffEntitlementDtoList consists of:
      | attribute     				| validation | type   |
      | accountId     				| required   | string |
      | partnerSaleRegistrationId 	| required   | number |
      | facilityName  				| required   | string |
      | invoiceNumber   			| required   | string |
      | invoiceUrl 					| optional   | string |
      | userId  					| required   | string |
      | installDate   				| required   | date   |
      | spiffAmount   				| required   | number |
      
  Scenario: Success
    Given I provide an accessToken identifying me as a partner rep
    And provide a valid spiffEntitlementDtoList
    When I execute createSpiffEntitlements
    Then spiffEntitlementDtoList is added to the spiffentitlement database with attributes:
      | attribute     			  | value                     			  		 	|
      | spiffEntitlementId        | (auto generated)          			  		 	|
      | accountId     			  | spiffEntitlementDto.accountId     				|
      | partnerSaleRegistrationId | spiffEntitlementDto.partnerSaleRegistrationId 	|
      | facilityName  			  | spiffEntitlementDto.facilityName  				|
      | invoiceNumber   		  | spiffEntitlementDto.invoiceNumber   			|
      | invoiceUrl     		 	  | spiffEntitlementDto.invoiceUrl     				|
      | userId 					  | spiffEntitlementDto.userId 						|
      | installDate  			  | spiffEntitlementDto.installDate  				|
      | spiffAmount   			  | spiffEntitlementDto.spiffAmount   				|

