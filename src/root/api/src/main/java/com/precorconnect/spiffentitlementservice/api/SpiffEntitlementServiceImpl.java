package com.precorconnect.spiffentitlementservice.api;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.spiffentitlementservice.core.Core;
import com.precorconnect.spiffentitlementservice.core.CoreImpl;
import com.precorconnect.spiffentitlementservice.database.DatabaseAdapterImpl;
import com.precorconnect.spiffentitlementservice.identity.IdentityServiceAdapterImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

public class SpiffEntitlementServiceImpl
		implements SpiffEntitlementService {

    /*
    fields
     */
    private final Core core;

    /*
    constructors
     */
    public SpiffEntitlementServiceImpl(
            @NonNull SpiffEntitlementServiceConfig config
    ) {

        core = new CoreImpl(
        					new DatabaseAdapterImpl(
        						config.getDatabaseAdapterConfig()
        						),
        					new IdentityServiceAdapterImpl(
        						config.getIdentityServiceAdapterConfig()
        						)
        				) ;

    }

	@Override
	public Collection<SpiffEntitlementId> createSpiffEntitlements(
			@NonNull List<SpiffEntitlementDto> spiffEntitlements,
			@NonNull OAuth2AccessToken accessToken
		    ) throws AuthenticationException, AuthorizationException {

		return
				core
				.createSpiffEntitlements(
						spiffEntitlements,
						accessToken
				 );
	}

	@Override
	public Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken accessToken
		    ) throws AuthenticationException, AuthorizationException {

		return
				core
				.listEntitlementsWithPartnerId(
						accountId,
						accessToken
				 );
	}

	@Override
	public void updateInvoiceUrl(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull InvoiceUrl invoiceUrl,
			@NonNull OAuth2AccessToken accessToken
		    ) throws AuthenticationException, AuthorizationException {

				core
					.updateInvoiceUrl(
							partnerSaleRegistrationId,
							invoiceUrl,
							accessToken
							);

	}

	@Override
	public void updatePartnerRep(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull UserId partnerRepUserid,
			@NonNull OAuth2AccessToken accessToken
		    ) throws AuthenticationException, AuthorizationException {

		core
			.updatePartnerRep(
					partnerSaleRegistrationId,
					partnerRepUserid,
					accessToken
					);

	}

	@Override
	public void deleteSpiffEntitlements(
			 @NonNull List<SpiffEntitlementId> spiffEntitlementIds,
			 @NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException, AuthorizationException {

		core
			.deleteSpiffEntitlements(
					spiffEntitlementIds,
					accessToken
					);
	}

}
