package com.precorconnect.spiffentitlementservice.api;

import com.precorconnect.spiffentitlementservice.database.DatabaseAdapterConfigFactoryImpl;
import com.precorconnect.spiffentitlementservice.identity.IdentityServiceAdapterConfigFactoryImpl;


public class SpiffEntitlementServiceConfigFactoryImpl implements
		SpiffEntitlementServiceConfigFactory {

	@Override
	public SpiffEntitlementServiceConfig construct() {
		
		return 
				new SpiffEntitlementServiceConfigImpl(
						new DatabaseAdapterConfigFactoryImpl()
												  .construct(),
						new IdentityServiceAdapterConfigFactoryImpl()
													.construct()
					);
	}

}
