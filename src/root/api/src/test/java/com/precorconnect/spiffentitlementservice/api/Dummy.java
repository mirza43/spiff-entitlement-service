package com.precorconnect.spiffentitlementservice.api;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.FacilityName;
import com.precorconnect.spiffentitlementservice.objectmodel.FacilityNameImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InstallDate;
import com.precorconnect.spiffentitlementservice.objectmodel.InstallDateImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceNumber;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffAmount;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDtoImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementViewImpl;

public class Dummy {

	/*
    fields
     */

	private URI uri;

	private AccountId accountId = new AccountIdImpl("109837372893488727");

	private PartnerSaleRegistrationId partnerSaleRegistrationId = new PartnerSaleRegistrationIdImpl(12L);

	private FacilityName facilityName = new FacilityNameImpl("Ram");

	private InvoiceNumber invoiceNumber = new InvoiceNumberImpl("1234");

	private InvoiceUrl invoiceUrl = new InvoiceUrlImpl("www.testinvoice.com");

	private UserId partnerRepUserId = new UserIdImpl("23");

	private InstallDate installDate ;

	private SpiffAmount spiffAmount = new SpiffAmountImpl(100.00);

	private SpiffEntitlementId spiffEntitlementId = new SpiffEntitlementIdImpl(1L);

	private SpiffEntitlementDto spiffEntitlementDto;

	private List<SpiffEntitlementDto> spiffEntitlementDtoList = new ArrayList<SpiffEntitlementDto>();

	private SpiffEntitlementView spiffEntitlementView;

	private Collection<SpiffEntitlementView> spiffEntitlementViewList = new ArrayList<SpiffEntitlementView>();

	private OAuth2AccessToken accessToken;

	private Collection<SpiffEntitlementId> spiffEntitlementIdList = new ArrayList<SpiffEntitlementId>();

	private InvoiceUrl newInvoiceUrl = new InvoiceUrlImpl("http://www.yahoo.com/test");

	private UserId newPartnerRepUserId = new UserIdImpl("222");

	public Dummy(){

		try {
			uri = new URI("http://dev.precorconnect.com");
		} catch (URISyntaxException e) {
			throw new RuntimeException("exception occured in uri creation: ",e);
		}

		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date date;
		try {
			date = formatter.parse("11/21/1985");
		} catch (ParseException e) {
			throw new RuntimeException("Date field parse exception: ",e);
		}
		installDate = new InstallDateImpl(date);

		 spiffEntitlementDto = new SpiffEntitlementDtoImpl(
				 									accountId,
				 									partnerSaleRegistrationId,
				 									facilityName,
				 									invoiceNumber,
				 									invoiceUrl,
				 									partnerRepUserId,
				 									installDate,
				 									spiffAmount
				 									);
		 spiffEntitlementView = new SpiffEntitlementViewImpl(
				 										spiffEntitlementId,
				 										accountId,
				 										partnerSaleRegistrationId,
				 										facilityName,
				 										invoiceNumber,
				 										invoiceUrl,
				 										partnerRepUserId,
				 										installDate,
				 										spiffAmount
				 										);

		 spiffEntitlementDtoList.add(spiffEntitlementDto);

		 spiffEntitlementIdList.add(spiffEntitlementId);

		 spiffEntitlementViewList.add(spiffEntitlementView);
	}

	public AccountId getAccountId() {
		return accountId;
	}

	public PartnerSaleRegistrationId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public FacilityName getFacilityName() {
		return facilityName;
	}

	public InvoiceNumber getInvoiceNumber() {
		return invoiceNumber;
	}

	public InvoiceUrl getInvoiceUrl() {
		return invoiceUrl;
	}

	public UserId getPartnerRepUserId() {
		return partnerRepUserId;
	}

	public InstallDate getInstallDate() {
		return installDate;
	}

	public SpiffAmount getSpiffAmount() {
		return spiffAmount;
	}

	public SpiffEntitlementId getSpiffEntitlementId() {
		return spiffEntitlementId;
	}

	public SpiffEntitlementDto getSpiffEntitlementDto() {
		return spiffEntitlementDto;
	}

	public List<SpiffEntitlementDto> getSpiffEntitlementDtoList() {
		return spiffEntitlementDtoList;
	}

	public OAuth2AccessToken getAccessToken() {
		return accessToken;
	}

	public Collection<SpiffEntitlementId> getSpiffEntitlementIdList() {
		return spiffEntitlementIdList;
	}

	public SpiffEntitlementView getSpiffEntitlementView() {
		return spiffEntitlementView;
	}

	public Collection<SpiffEntitlementView> getSpiffEntitlementViewList() {
		return spiffEntitlementViewList;
	}

	public InvoiceUrl getNewInvoiceUrl() {
		return newInvoiceUrl;
	}

	public UserId getNewPartnerRepUserId() {
		return newPartnerRepUserId;
	}

	public URI getUri() {
		return uri;
	}

}
