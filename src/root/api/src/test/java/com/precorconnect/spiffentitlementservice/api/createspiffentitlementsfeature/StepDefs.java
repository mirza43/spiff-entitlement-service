package com.precorconnect.spiffentitlementservice.api.createspiffentitlementsfeature;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.List;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;
import com.precorconnect.spiffentitlementservice.api.Config;
import com.precorconnect.spiffentitlementservice.api.ConfigFactory;
import com.precorconnect.spiffentitlementservice.api.Dummy;
import com.precorconnect.spiffentitlementservice.api.Factory;
import com.precorconnect.spiffentitlementservice.api.SpiffEntitlementService;
import com.precorconnect.spiffentitlementservice.api.SpiffEntitlementServiceImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs {

	/*
	 fields
	 */
	private final Config config = new ConfigFactory()
										.construct();

	private final Dummy dummy = new Dummy();

	private final Factory factory =
            new Factory(
            		dummy,
                    new IdentityServiceIntegrationTestSdkImpl(
                    			config
                                      .getIdentityServiceJwtSigningKey()
                    )
            );

	List<SpiffEntitlementDto> spiffEntitlementDtoList ;

	private OAuth2AccessToken accessToken;

	private Collection<SpiffEntitlementId> spiffEntitlementIdList;


	@Given("^a spiffEntitlementDtoList consists of:$")
	public void aspiffEntitlementDtoListconsistsof(
			DataTable arg1
			) throws Throwable {
		// no op, we are creating spiffentitlementdtolist in dummy
	}

    @Given("^I provide an accessToken identifying me as a partner rep$")
    public void provideAnAccessTokenIdentifyingMeAsAPartnerRep(
    ) throws Throwable {

        accessToken = new OAuth2AccessTokenImpl(
                								factory
                									.constructValidAppOAuth2AccessToken()
                									.getValue()
                								);

    }

    @Given("^provide a valid spiffEntitlementDtoList$")
    public void provideAValidSpiffEntitlementDtoList(
    ) throws Throwable {

    	spiffEntitlementDtoList =
                	dummy.getSpiffEntitlementDtoList();

    }

    @When("^I execute createSpiffEntitlements$")
    public void iExecuteCreateSpiffEntitlements(
    ) throws Throwable {

        SpiffEntitlementService objectUnderTest =
                					new SpiffEntitlementServiceImpl(
                							config.getSpiffEntitlementServiceConfig()
                							);

        spiffEntitlementIdList = objectUnderTest
                						.createSpiffEntitlements(
                								spiffEntitlementDtoList,
                								accessToken
                								);

    }

    @Then("^spiffEntitlementDtoList is added to the spiffentitlement database with attributes:$")
    public void anspiffentitlementisaddedtothespiffentitlementdatabase(
    		DataTable arg
    ) throws Throwable {

    	assertEquals(
				dummy.getSpiffEntitlementIdList().size(),
				spiffEntitlementIdList.size()
				);

    }


}
