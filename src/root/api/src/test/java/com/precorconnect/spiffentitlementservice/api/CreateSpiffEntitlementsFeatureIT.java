package com.precorconnect.spiffentitlementservice.api;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"features/CreateSpiffEntitlements.feature"},
        glue = {"com.precorconnect.spiffentitlementservice.api.createspiffentitlementsfeature"}
        )
public class CreateSpiffEntitlementsFeatureIT {

}
