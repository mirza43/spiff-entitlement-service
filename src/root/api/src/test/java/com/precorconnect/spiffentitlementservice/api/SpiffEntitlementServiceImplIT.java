package com.precorconnect.spiffentitlementservice.api;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.Collection;

import org.junit.Test;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

public class SpiffEntitlementServiceImplIT {

    /*
    fields
     */
	private Dummy dummy = new Dummy();

	private Config config = new ConfigFactory()
										.construct();

	private final Factory factory =
            new Factory(
            		dummy,
                    new IdentityServiceIntegrationTestSdkImpl(
                    			config
                                      .getIdentityServiceJwtSigningKey()
                    )
            );

	private SpiffEntitlementServiceImpl spiffEntitlementService  =
											new SpiffEntitlementServiceImpl(
													config.getSpiffEntitlementServiceConfig()
													);

	/*
    test methods
	*/
	@Test
	public void testCreateSpiffEntitlements_whenSpiffEntitlementDtoList_shownSpiffEntitlementIdList() throws AuthenticationException, AuthorizationException {

		OAuth2AccessToken accessToken = new OAuth2AccessTokenImpl(
												factory
													.constructValidAppOAuth2AccessToken()
													.getValue()
												);

		Collection<SpiffEntitlementId> spiffEntitlementIdList = spiffEntitlementService
																		.createSpiffEntitlements(
																				dummy.getSpiffEntitlementDtoList(),
																				accessToken
																				);
		assertEquals(
				dummy.getSpiffEntitlementIdList().size(),
				spiffEntitlementIdList.size()
				);
	}

	@Test
	public void testlistEntitlementsWithPartnerId_whenAccountId_shownSpiffEntitlementList() throws AuthenticationException, AuthorizationException {

		OAuth2AccessToken accessToken = new OAuth2AccessTokenImpl(
												factory
													.constructValidAppOAuth2AccessToken()
													.getValue()
												);

		Collection<SpiffEntitlementView> spiffEntitlementViewList = spiffEntitlementService
																		.listEntitlementsWithPartnerId(
																				dummy.getAccountId(),
																				accessToken
																				);

		assertThat(spiffEntitlementViewList.size())
					.isGreaterThan(0);
	}

	@Test
	public void testupdateInvoiceUrl_whenPartnerSaleRegId_shownNothing() throws AuthenticationException, AuthorizationException {

		OAuth2AccessToken accessToken = new OAuth2AccessTokenImpl(
												factory
													.constructValidAppOAuth2AccessToken()
													.getValue()
												);

		spiffEntitlementService
						.updateInvoiceUrl(
								dummy.getPartnerSaleRegistrationId(),
								dummy.getNewInvoiceUrl(),
								accessToken
								);

	}

	@Test
	public void testupdatePartnerRep_whenPartnerSaleRegId_shownNothing() throws AuthenticationException, AuthorizationException {

		OAuth2AccessToken accessToken = new OAuth2AccessTokenImpl(
												factory
													.constructValidAppOAuth2AccessToken()
													.getValue()
												);

		spiffEntitlementService
						.updatePartnerRep(
								dummy.getPartnerSaleRegistrationId(),
								dummy.getNewPartnerRepUserId(),
								accessToken
								);

	}

}
