package com.precorconnect.spiffentitlementservice.api;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.identityservice.HmacKey;

public class Config {

	/*
	 fields
	*/
	private final SpiffEntitlementServiceConfig spiffEntitlementServiceConfig;

    private final HmacKey identityServiceJwtSigningKey;

    private final String baseUrl;

    /*
    constructors
     */
    public Config(
    	@NonNull final SpiffEntitlementServiceConfig spiffEntitlementServiceConfig,
    	@NonNull final HmacKey identityServiceJwtSigningKey,
		@NonNull final String baseUrl
    		) {

    	this.spiffEntitlementServiceConfig =
                guardThat("spiffEntitlementServiceConfig",
                		spiffEntitlementServiceConfig
                )
                        .isNotNull()
                        .thenGetValue();

    	this.identityServiceJwtSigningKey = identityServiceJwtSigningKey;

    	this.baseUrl = baseUrl;

    }

    /*
    getter methods
    */
    public SpiffEntitlementServiceConfig getSpiffEntitlementServiceConfig() {

        return spiffEntitlementServiceConfig;

    }

    public HmacKey getIdentityServiceJwtSigningKey() {
        return identityServiceJwtSigningKey;
    }

	public String getBaseUrl() {
		return baseUrl;
	}
}
