package com.precorconnect.spiffentitlementservice.core;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

@RunWith(MockitoJUnitRunner.class)
public class ListSpiffEntitlementFeatureImplTest {
	/*
    fields
     */
	@Mock
	private DatabaseAdapter databaseAdapter;

	@InjectMocks
	private ListSpiffEntitlementFeatureImpl listSpiffEntitlementFeatureImpl;

	private Dummy dummy = new Dummy();

	@Test
	public void testlistEntitlementsWithPartnerId_whenAccountId_shownSpiffEntitlementList() throws AuthenticationException, AuthorizationException {

		when(databaseAdapter
				.listEntitlementsWithPartnerId(
						dummy.getAccountId()
						)
			)
			.thenReturn(
						dummy.getSpiffEntitlementViewList()
						);

		Collection<SpiffEntitlementView> spiffEntitlementViewList = listSpiffEntitlementFeatureImpl
																		.listEntitlementsWithPartnerId(
																				dummy.getAccountId()
																				);

		assertThat(spiffEntitlementViewList.size())
        			.isGreaterThan(0);
	}
}
