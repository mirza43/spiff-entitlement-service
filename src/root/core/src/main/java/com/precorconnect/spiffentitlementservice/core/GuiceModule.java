package com.precorconnect.spiffentitlementservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;



class GuiceModule extends
        AbstractModule {

    private final DatabaseAdapter databaseAdapter;

    private final IdentityServiceAdapter identityServiceAdapter;

    public GuiceModule(
            @NonNull final DatabaseAdapter databaseAdapter,
            @NonNull final IdentityServiceAdapter identityServiceAdapter
            ){
    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                        databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.identityServiceAdapter =
                guardThat(
                        "identityServiceAdapter",
                        identityServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure() {

        bindAdapters();
        bindFeatures();

    }

    private void bindAdapters() {

        bind(DatabaseAdapter.class)
                .toInstance(databaseAdapter);

        bind(IdentityServiceAdapter.class)
        		.toInstance(identityServiceAdapter);

    }

    private void bindFeatures() {

        bind(ListSpiffEntitlementFeature.class)
                .to(ListSpiffEntitlementFeatureImpl.class);

        bind(CreateSpiffEntitlementFeature.class)
        		.to(CreateSpiffEntitlementFeatureImpl.class);

        bind(UpdatePartnerRepFeature.class)
        		.to(UpdatePartnerRepFeatureImpl.class);

        bind(UpdateInvoiceUrlFeature.class)
				.to(UpdateInvoiceUrlFeatureImpl.class);

        bind(DeleteSpiffEntitlementsFeature.class)
        		.to(DeleteSpiffEntitlementsFeatureImpl.class);

    }
}
