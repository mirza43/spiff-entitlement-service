package com.precorconnect.spiffentitlementservice.core;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;

public interface CreateSpiffEntitlementFeature {

	 Collection<SpiffEntitlementId> createSpiffEntitlements(
										@NonNull List<SpiffEntitlementDto> spiffEntitlements
									) throws AuthenticationException, AuthorizationException;

}
