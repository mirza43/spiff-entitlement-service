package com.precorconnect.spiffentitlementservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

@Singleton
public class ListSpiffEntitlementFeatureImpl implements
		ListSpiffEntitlementFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public ListSpiffEntitlementFeatureImpl(
            @NonNull final DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
				@NonNull AccountId accountId
			) throws AuthenticationException, AuthorizationException{


		return
                databaseAdapter.listEntitlementsWithPartnerId(accountId);
	}

}
