package com.precorconnect.spiffentitlementservice.core;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

public class CoreImpl implements Core {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public CoreImpl(
            @NonNull DatabaseAdapter databaseAdapter,
            @NonNull IdentityServiceAdapter identityServiceAdapter
    ) {

    	GuiceModule guiceModule =
                new GuiceModule(
                        databaseAdapter,
                        identityServiceAdapter
                        );


        injector = Guice.createInjector(guiceModule);
    }

	@Override
	public Collection<SpiffEntitlementId> createSpiffEntitlements(
			@NonNull List<SpiffEntitlementDto> spiffEntitlementDtos,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {

		injector
			.getInstance(IdentityServiceAdapter.class)
			.getPartnerRepAccessContext(accessToken);

		return
                injector
                        .getInstance(CreateSpiffEntitlementFeature.class)
                        	.createSpiffEntitlements(
                        			spiffEntitlementDtos
                        			);
	}

	@Override
	public Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {

		injector
			.getInstance(IdentityServiceAdapter.class)
			.getPartnerRepAccessContext(accessToken);

		return
                injector
                        .getInstance(ListSpiffEntitlementFeature.class)
                        	.listEntitlementsWithPartnerId(
                        			accountId
                        			);
	}

	@Override
	public void updateInvoiceUrl(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull InvoiceUrl invoiceUrl,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {

    	injector
			.getInstance(IdentityServiceAdapter.class)
			.getPartnerRepAccessContext(accessToken);

                injector
                        .getInstance(UpdateInvoiceUrlFeature.class)
                        	.updateInvoiceUrl(
                        			partnerSaleRegistrationId,
                        			invoiceUrl
                        			);
	}

	@Override
	public void updatePartnerRep(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull UserId partnerRepUserId,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {

    	injector
			.getInstance(IdentityServiceAdapter.class)
			.getPartnerRepAccessContext(accessToken);

                injector
                        .getInstance(UpdatePartnerRepFeature.class)
                        	.updatePartnerRep(
                        			partnerSaleRegistrationId,
                        			partnerRepUserId
                        			);


	}

	@Override
	public void deleteSpiffEntitlements(
		 	@NonNull List<SpiffEntitlementId> spiffEntitlementIds,
		 	@NonNull OAuth2AccessToken accessToken
		 ) throws AuthenticationException, AuthorizationException {

		injector
			.getInstance(IdentityServiceAdapter.class)
			.getPartnerRepAccessContext(accessToken);

		injector
        	.getInstance(DeleteSpiffEntitlementsFeature.class)
        		.deleteSpiffEntitlements(
        				spiffEntitlementIds
        				);
	}

}
