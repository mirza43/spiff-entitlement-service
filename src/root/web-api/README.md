## Description
Precor Connect spiff entitlement service API for ReST.

## Features

##### Create Spiff Entitlements
* [documentation](features/CreateSpiffEntitlements.feature)

##### List Spiff Entitlements With Id
* [documentation](features/ListEntitlementsWithPartnerId.feature)

##### Update Invoice Url
* [documentation](features/UpdateInvoiceUrl.feature)

##### Update Partner Rep
* [documentation](features/UpdatePartnerRep.feature)

## API Explorer

##### Environments:
-  [dev](https://spiff-entitlement-service-dev.precorconnect.com/)
-  [qa](https://spiff-entitlement-service-qa.precorconnect.com/)
-  [prod](https://spiff-entitlement-service-prod.precorconnect.com/)