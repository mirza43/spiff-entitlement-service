package com.precorconnect.spiffentitlementservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffentitlementservice.webapiobjectmodel.SpiffEntitlementWebView;

public interface SpiffEntitlementsWebResponseFactory {

    SpiffEntitlementWebView construct(
    		  @NonNull SpiffEntitlementView spiffEntitlementView
    );

}
