package com.precorconnect.spiffentitlementservice.webapi;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffentitlementservice.webapiobjectmodel.SpiffEntitlementWebDto;

public class Dummy {

    /*
    fields
     */
    private URI uri;

    private String accountId;

    private SpiffEntitlementWebDto spiffEntitlementWebDto;

    private List<SpiffEntitlementWebDto> spiffEntitlementWebDtos = new ArrayList<SpiffEntitlementWebDto>();

    private OAuth2AccessToken accessToken = null;

    private String newInvoiceUrl ;

    private String newUserId;

    /*
    constructors
     */
    public Dummy() {

        try {

            uri = new URI("http://dev.precorconnect.com");

            accountId = "109837372893488727";

            newInvoiceUrl = "http://www.yahoo.com/test";

            newUserId = "222";

            spiffEntitlementWebDto = new SpiffEntitlementWebDto(
            													"109837372893488727",
            													131L,
            													"newfacility",
            													"231123",
            													"testurl",
            													"23",
            													"11/21/2000",
            													100.00
            													);

            spiffEntitlementWebDtos.add(spiffEntitlementWebDto);

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }

    }

    /*
    getter methods
     */
    public URI getUri() {

        return uri;

    }

    public String getAccountId() {

        return accountId;

    }

	public SpiffEntitlementWebDto getSpiffEntitlementWebDto() {
		return spiffEntitlementWebDto;
	}

	public List<SpiffEntitlementWebDto> getSpiffEntitlementWebDtos() {
		return spiffEntitlementWebDtos;
	}

	public OAuth2AccessToken getAccessToken() {
		return accessToken;
	}

	public String getNewInvoiceUrl() {
		return newInvoiceUrl;
	}

	public String getNewUserId() {
		return newUserId;
	}


}
