package com.precorconnect.spiffentitlementservice.webapi.listentitlementswithpartneridfeature;

import static com.jayway.restassured.RestAssured.given;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;
import com.precorconnect.spiffentitlementservice.webapi.AbstractSpringIntegrationTest;
import com.precorconnect.spiffentitlementservice.webapi.Config;
import com.precorconnect.spiffentitlementservice.webapi.ConfigFactory;
import com.precorconnect.spiffentitlementservice.webapi.Dummy;
import com.precorconnect.spiffentitlementservice.webapi.Factory;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs
		extends AbstractSpringIntegrationTest{

    /*
    fields
     */
    private final Config config =
            new ConfigFactory().construct();

    private final Dummy dummy = new Dummy();

    private final Factory factory =
            new Factory(
                    dummy,
                    new IdentityServiceIntegrationTestSdkImpl(
                            config.getIdentityServiceJwtSigningKey()
                    )
            );

    private String accountId;

    private OAuth2AccessToken oAuth2AccessToken;

    private Response responseOfPostToSpiffEntitlements;

    @Before
    public void beforeAll() {

        RestAssured.port = getPort();

    }

    /*
    steps
     */
    @Given("^an accountId consists of:$")
    public void anaccountIdconsistsof(
            DataTable attributes
    ) throws Throwable {

        // no op , input prepared in dummy

    }

    @Given("^I provide an accessToken identifying me as a partner rep$")
    public void provideanaccessTokenidentifyingmeasapartnerrep(
    ) throws Throwable {

        oAuth2AccessToken =
                factory.constructValidAppOAuth2AccessToken();

    }

    @And("^provide a valid accountId$")
    public void provideavalidaccountId(
    ) throws Throwable {

    	accountId =
                dummy.getAccountId();

    }

    @When("^I make a get request to /spiff-entitlements$")
    public void makeagetrequesttospiffentitlements(
    		) throws Throwable {
    	responseOfPostToSpiffEntitlements =
    			given()
                .contentType(ContentType.JSON)
                .header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                oAuth2AccessToken
                                        .getValue()
                        )
                )
                .get("/spiff-entitlements"
                		+accountId);

    }

    @Then("^the spiffentitlements with the matched accountid are returned$")
    public void thespiffentitlementswiththematchedaccountidarereturned(
    		){

    	responseOfPostToSpiffEntitlements
    			.then()
    			.assertThat()
    			.statusCode(200);

    }
}
