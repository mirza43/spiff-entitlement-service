package com.precorconnect.spiffentitlementservice.webapi;

import static com.jayway.restassured.RestAssured.given;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jayway.restassured.RestAssured;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest({"server.port=0"})
public class SpiffEntitlementResourceIT {

    /*
    fields
     */
	private final Config config=
						 new ConfigFactory()
    									 .construct();

	Dummy dummy = new Dummy();

    private final Factory factory =
            new Factory(
            		dummy,
                    new IdentityServiceIntegrationTestSdkImpl(
                    			config
                                      .getIdentityServiceJwtSigningKey()
                    )
            );

    @Value("${local.server.port}")
    int port;

    @Before
    public void setUp() {

        RestAssured.port = port;
    }

    @Test
    public void createSpiffEntitlements_ReturnsListOfIds() {

        given()
                .header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                factory
                                        .constructValidAppOAuth2AccessToken()
                                        .getValue()
                        )
                )
        		.header(
        				"Content-Type",
        				"application/json"
        				)
                .body(dummy.getSpiffEntitlementWebDtos())
                .post("/spiff-entitlements")
                .then()
                .assertThat()
                .statusCode(200);

    }

    @Test
    public void listEntitlementsWithPartnerId_ReturnsListofEntitlements() {

        given()
                .header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                factory
                                        .constructValidAppOAuth2AccessToken()
                                        .getValue()
                        )
                )
                .get("/spiff-entitlements/"
                		+dummy.getAccountId()
                )
                .then()
                .assertThat()
                .statusCode(200);

    }

    @Test
    public void updateInvoiceUrl_ReturnsNothing() {

        given()
                .header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                factory
                                        .constructValidAppOAuth2AccessToken()
                                        .getValue()
                        )
                )
        		.body(dummy.getNewInvoiceUrl())
                .post("/spiff-entitlements/"
                		+dummy.getSpiffEntitlementWebDto().getPartnerSaleRegistrationId()
                		+"/invoiceurl"
                )
                .then()
                .assertThat()
                .statusCode(200);

    }

    @Test
    public void updatePartnerRep_ReturnsNothing() {

        given()
                .header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                factory
                                        .constructValidAppOAuth2AccessToken()
                                        .getValue()
                        )
                )
                .post("/spiff-entitlements/"
                		+ dummy.getSpiffEntitlementWebDto().getPartnerSaleRegistrationId()
                		+ "/partnerrepuserid/"
                		+ dummy.getNewUserId()
                )
                .then()
                .assertThat()
                .statusCode(200);

    }

}
