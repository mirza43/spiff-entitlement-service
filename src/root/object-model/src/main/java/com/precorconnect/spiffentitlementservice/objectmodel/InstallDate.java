package com.precorconnect.spiffentitlementservice.objectmodel;

import java.util.Date;

public interface InstallDate {

	public Date getValue();

}
