package com.precorconnect.spiffentitlementservice.objectmodel;

public interface SpiffEntitlementId {

	public Long getValue();

}
