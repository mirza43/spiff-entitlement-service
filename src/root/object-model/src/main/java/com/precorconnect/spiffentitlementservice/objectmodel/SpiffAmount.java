package com.precorconnect.spiffentitlementservice.objectmodel;

public interface SpiffAmount {

	public Double getValue();

}
