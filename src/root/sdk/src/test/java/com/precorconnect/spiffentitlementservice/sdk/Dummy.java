package com.precorconnect.spiffentitlementservice.sdk;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.precorconnect.AccountIdImpl;
import com.precorconnect.UserIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.FacilityNameImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InstallDateImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDtoImpl;
import com.precorconnect.spiffentitlementservice.webapiobjectmodel.SpiffEntitlementWebDto;

public class Dummy {

    /*
    fields
     */
    private URI uri;

    private SpiffEntitlementDto spiffEntitlementDto;

    private SpiffEntitlementWebDto spiffEntitlementWebDto;

    private List<SpiffEntitlementDto> spiffEntitlementDtosList = new ArrayList<SpiffEntitlementDto>();

    private List<SpiffEntitlementWebDto> spiffEntitlementWebDtosList = new ArrayList<SpiffEntitlementWebDto>();

    private Date date;

    private String newInvoiceUrl;

    private String newUserId;



	/*
    constructors
     */
    public Dummy() {

        try {

            uri = new URI("http://dev.precorconnect.com");

            try {
				date = new SimpleDateFormat("MM/dd/yyyy").parse("11/21/1999");
			} catch (ParseException e) {
				throw new RuntimeException("date parsing exception: ", e);
			}

            newInvoiceUrl = "http://www.yahoo.com/test";

            newUserId = "222";

            spiffEntitlementDto = new
            						SpiffEntitlementDtoImpl(
            								new AccountIdImpl("109837372893488727"),
            								new PartnerSaleRegistrationIdImpl(12L),
            								new FacilityNameImpl("john"),
            								new InvoiceNumberImpl("1234"),
            								new InvoiceUrlImpl("invoiceurl1234"),
            								new UserIdImpl("23"),
            								new InstallDateImpl(date),
            								new SpiffAmountImpl(100.00)
            								);

            spiffEntitlementDtosList.add(spiffEntitlementDto);

            spiffEntitlementWebDto = new SpiffEntitlementWebDto(
            												"109837372893488727",
            												12L,
            												"Ram",
            												"1234",
            												"invoiceUrl1234",
            												"23",
            												"11/21/1985",
            												100.00
            												);

            spiffEntitlementWebDtosList.add(spiffEntitlementWebDto);


        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }

    }

    /*
    getter methods
     */
    public URI getUri() {
        return uri;
    }

    public SpiffEntitlementDto getSpiffEntitlementDto() {
		return spiffEntitlementDto;
	}

	public List<SpiffEntitlementDto> getSpiffEntitlementDtosList() {
		return spiffEntitlementDtosList;
	}

	public SpiffEntitlementWebDto getSpiffEntitlementWebDto() {
		return spiffEntitlementWebDto;
	}

	public List<SpiffEntitlementWebDto> getSpiffEntitlementWebDtosList() {
		return spiffEntitlementWebDtosList;
	}

	public Date getDate() {
		return date;
	}

	public String getNewInvoiceUrl() {
		return newInvoiceUrl;
	}

	public String getNewUserId() {
		return newUserId;
	}

}
