package com.precorconnect.spiffentitlementservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.webapiobjectmodel.SpiffEntitlementWebDto;

@Singleton
public class SpiffEntitlementsSdkRequestFactoryImpl implements
		SpiffEntitlementsSdkRequestFactory {

	@Override
	public SpiffEntitlementWebDto construct(
			@NonNull SpiffEntitlementDto spiffEntitlementDto) {

		guardThat(
                "spiffEntitlementDto",
                spiffEntitlementDto
				)
                .isNotNull();

		String accountId =
					spiffEntitlementDto
					.getAccountId()
					.getValue();

		Long partnerSaleRegistrationId =
					spiffEntitlementDto
					.getPartnerSaleRegistrationId()
					.getValue();

		String facilityName =
					spiffEntitlementDto
					.getFacilityName()
					.getValue();

		String invoiceNumber =
					spiffEntitlementDto
					.getInvoiceNumber()
					.getValue();

		String invoiceUrl =
					spiffEntitlementDto
					.getInvoiceUrl()
					.getValue();

		String partnerRepUserId =
					spiffEntitlementDto
					.getPartnerRepUserId()
					.getValue();

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

		String installDate = dateFormat.format(
										spiffEntitlementDto
										.getInstallDate()
										.getValue()
										);

		Double spiffAmount =
					spiffEntitlementDto
					.getSpiffAmount()
					.getValue();

		return new SpiffEntitlementWebDto(
										accountId,
										partnerSaleRegistrationId,
										facilityName,
										invoiceNumber,
										invoiceUrl,
										partnerRepUserId,
										installDate,
										spiffAmount
										);
	}

}
