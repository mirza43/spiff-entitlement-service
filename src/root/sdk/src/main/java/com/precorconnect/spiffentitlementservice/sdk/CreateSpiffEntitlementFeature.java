 package com.precorconnect.spiffentitlementservice.sdk;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;

public interface CreateSpiffEntitlementFeature {

	public Collection<Long> createSpiffEntitlements(
			@NonNull List<SpiffEntitlementDto> spiffEntitlements,
			@NonNull OAuth2AccessToken oAuth2AccessToken
			) throws AuthenticationException, AuthorizationException;

}
