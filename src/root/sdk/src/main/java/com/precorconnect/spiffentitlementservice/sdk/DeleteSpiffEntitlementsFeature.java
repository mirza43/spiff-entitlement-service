package com.precorconnect.spiffentitlementservice.sdk;

import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;

public interface DeleteSpiffEntitlementsFeature {

	void deleteSpiffEntitlements(
			@NonNull List<SpiffEntitlementId> spiffEntitlementIds,
			@NonNull OAuth2AccessToken oAuth2AccessToken
			) throws AuthenticationException, AuthorizationException;

}
