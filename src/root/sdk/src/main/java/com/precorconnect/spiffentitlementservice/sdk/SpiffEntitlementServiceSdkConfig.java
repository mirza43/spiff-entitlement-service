package com.precorconnect.spiffentitlementservice.sdk;

import java.net.URL;

public interface SpiffEntitlementServiceSdkConfig {

	URL getPrecorConnectApiBaseUrl();
	
}
