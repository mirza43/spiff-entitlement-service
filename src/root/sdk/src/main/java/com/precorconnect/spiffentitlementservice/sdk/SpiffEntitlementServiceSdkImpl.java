package com.precorconnect.spiffentitlementservice.sdk;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

public class SpiffEntitlementServiceSdkImpl implements
		SpiffEntitlementServiceSdk {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public SpiffEntitlementServiceSdkImpl(
            @NonNull SpiffEntitlementServiceSdkConfig config
    ) {

        GuiceModule guiceModule =
                new GuiceModule(
                        config
                );

        injector =
                Guice.createInjector(guiceModule);
    }

	@Override
	public Collection<Long> createSpiffEntitlements(
			@NonNull List<SpiffEntitlementDto> spiffEntitlements,
			@NonNull OAuth2AccessToken accessToken)
					throws AuthenticationException, AuthorizationException {

		return
                injector
                        .getInstance(CreateSpiffEntitlementFeature.class)
                        .createSpiffEntitlements(
                        		spiffEntitlements,
                        		accessToken
                        		);

	}

	@Override
	public Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken accessToken)
					throws AuthenticationException, AuthorizationException {

		return
                injector
                        .getInstance(ListSpiffEntitlementFeature.class)
                        .listEntitlementsWithPartnerId(
                        		accountId,
                        		accessToken
                        		);

	}

	@Override
	public void updateInvoiceUrl(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull InvoiceUrl invoiceUrl,
			@NonNull OAuth2AccessToken accessToken)
					throws AuthenticationException, AuthorizationException {

		injector
        		.getInstance(UpdateInvoiceUrlFeature.class)
        		.updateInvoiceUrl(
        				partnerSaleRegistrationId,
        				invoiceUrl,
        				accessToken
        				);

	}

	@Override
	public void updatePartnerRep(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull UserId partnerRepUserid,
			@NonNull OAuth2AccessToken accessToken)
					throws AuthenticationException, AuthorizationException {

		injector
				.getInstance(UpdatePartnerRepFeature.class)
				.updatePartnerRep(
						partnerSaleRegistrationId,
						partnerRepUserid,
						accessToken
						);

	}

	@Override
	public void deleteSpiffEntitlements(
			@NonNull List<SpiffEntitlementId> spiffEntitlementIds,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException, AuthorizationException {

		injector
				.getInstance(DeleteSpiffEntitlementsFeature.class)
				.deleteSpiffEntitlements(
							spiffEntitlementIds,
							accessToken
							);

	}

}
