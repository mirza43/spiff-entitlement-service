package com.precorconnect.spiffentitlementservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;

public final class InstrumentedDeleteSpiffEntitlementsFeatureImpl implements DeleteSpiffEntitlementsFeature {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(InstrumentedDeleteSpiffEntitlementsFeatureImpl.class);
	
	private final DeleteSpiffEntitlementsFeature underlyingFeature;
	
	
	@Inject
	public InstrumentedDeleteSpiffEntitlementsFeatureImpl(@UnderlyingFeature final DeleteSpiffEntitlementsFeature underlyingFeature) {
	
		this.underlyingFeature =
                guardThat(
                        "underlyingFeature",
                        underlyingFeature
                )
                        .isNotNull()
                        .thenGetValue();
	}



	@Override
	public void deleteSpiffEntitlements(@NonNull List<SpiffEntitlementId> spiffEntitlementIds,
			@NonNull OAuth2AccessToken oAuth2AccessToken) throws AuthenticationException, AuthorizationException {
		
		long startTime = System.currentTimeMillis();
		
		underlyingFeature.deleteSpiffEntitlements(
										spiffEntitlementIds, 
										oAuth2AccessToken
									);
		
		long endTime = System.currentTimeMillis();

		LOGGER.debug(String
				.format("[In sdk deleteSpiffEntitlements call took %s millis.[start=%s, end=%s]",
						(endTime - startTime), new Date(startTime), new Date(
								endTime)));

		
	}



	

	
}
