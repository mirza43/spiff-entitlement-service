package com.precorconnect.spiffentitlementservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.FacilityName;
import com.precorconnect.spiffentitlementservice.objectmodel.FacilityNameImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InstallDate;
import com.precorconnect.spiffentitlementservice.objectmodel.InstallDateImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceNumber;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffAmount;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementViewImpl;
import com.precorconnect.spiffentitlementservice.webapiobjectmodel.SpiffEntitlementWebView;

@Singleton
public class SpiffEntitlementsSdkResponseFactoryImpl implements
		SpiffEntitlementsSdkResponseFactory {

	@Override
	public SpiffEntitlementView construct(
			@NonNull SpiffEntitlementWebView spiffEntitlementWebView) {

		guardThat(
                "spiffEntitlementWebView",
                spiffEntitlementWebView
				)
                .isNotNull();

		SpiffEntitlementId spiffEntitlementId = new
												  SpiffEntitlementIdImpl(
															spiffEntitlementWebView
																.getSpiffEntitlementId()
															);

		AccountId accountId = new
											  AccountIdImpl(
														spiffEntitlementWebView
															.getAccountId()
															);

		PartnerSaleRegistrationId partnerSaleRegistrationId = new
																PartnerSaleRegistrationIdImpl(
																			spiffEntitlementWebView
																				.getPartnerSaleRegistrationId()
																				);

		FacilityName facilityName = new
									  FacilityNameImpl(
												spiffEntitlementWebView
													.getFacilityName()
													);

		InvoiceNumber invoiceNumber = new
										InvoiceNumberImpl(
												spiffEntitlementWebView
													.getInvoiceNumber()
													);

		InvoiceUrl invoiceUrl = new
								  InvoiceUrlImpl(
										  spiffEntitlementWebView
										  	.getInvoiceUrl()
										  	);

		UserId partnerRepUserId = new
				UserIdImpl(
													  	spiffEntitlementWebView
													  		.getPartnerRepUserId()
													  		);

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date;
		try {
			date = dateFormat.parse(
						spiffEntitlementWebView
							.getInstallDate());
			} catch (ParseException e) {
				throw new RuntimeException("install date parse exception: ", e);
			}

		InstallDate installDate = new InstallDateImpl(date);

		SpiffAmount spiffAmount = new
									SpiffAmountImpl(
											spiffEntitlementWebView
												.getSpiffAmount()
												);


		return
				new SpiffEntitlementViewImpl(
									spiffEntitlementId,
									accountId,
									partnerSaleRegistrationId,
									facilityName,
									invoiceNumber,
									invoiceUrl,
									partnerRepUserId,
									installDate,
									spiffAmount
						);
	}

}
