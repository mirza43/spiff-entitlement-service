package com.precorconnect.spiffentitlementservice.sdk;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;

public interface UpdateInvoiceUrlFeature {

	public void updateInvoiceUrl(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull InvoiceUrl invoiceUrl,
			@NonNull OAuth2AccessToken oAuth2AccessToken
			) throws AuthenticationException, AuthorizationException;

}
