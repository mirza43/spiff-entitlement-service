package com.precorconnect.spiffentitlementservice.sdk;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.webapiobjectmodel.SpiffEntitlementWebDto;

public interface SpiffEntitlementsSdkRequestFactory {

	public SpiffEntitlementWebDto construct(
				@NonNull SpiffEntitlementDto spiffEntitlementDto
			);

}
