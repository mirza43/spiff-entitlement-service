package com.precorconnect.spiffentitlementservice.database;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.junit.Assert.assertSame;

import java.net.URI;

import org.junit.Test;

import com.precorconnect.Password;
import com.precorconnect.Username;

public class DatabaseAdapterConfigImplTest {

	/*
    fields
	*/
    private final Dummy dummy = new Dummy();

    /*
    test methods
	*/
    @Test(expected = IllegalArgumentException.class)
    public void testConstructor_whenUriNull_shouldThrowIllegalArgumentException(
    ) throws Exception {

        new DatabaseAdapterConfigImpl(
                null,
                dummy.getUsername(),
                dummy.getPassword()
        );

    }

    @Test
    public void testConstructor_whenPassDummyUri_shouldSetDummyUri(
    ) throws Exception {

        URI expectedUri = dummy.getUri();

        DatabaseAdapterConfig objectUnderTest =
                new DatabaseAdapterConfigImpl(
                        expectedUri,
                        dummy.getUsername(),
                        dummy.getPassword()
                );

        URI actualUri =
                objectUnderTest.getUri();

        assertSame(expectedUri, actualUri);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructor_WhenUsernameNull_shouldThrowIllegalArgumentException(
    ) throws Exception {

        new DatabaseAdapterConfigImpl(
                dummy.getUri(),
                null,
                dummy.getPassword()
        );

    }

    @Test
    public void testConstructor_whenPassDummyUsername_shouldSetThatUsername(
    ) throws Exception {



        Username expectedUsername = dummy.getUsername();

        DatabaseAdapterConfig objectUnderTest =
                new DatabaseAdapterConfigImpl(
                        dummy.getUri(),
                        expectedUsername,
                        dummy.getPassword()
                );

        Username actualUsername =
                objectUnderTest.getUsername();

        assertThat(expectedUsername)
                .isSameAs(actualUsername);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructor_whenPasswordNull_shouldThrowsIllegalArgumentException(
    ) throws Exception {

        new DatabaseAdapterConfigImpl(
                dummy.getUri(),
                dummy.getUsername(),
                null
        );

    }

    @Test
    public void testConstructor_whenPassDummyPassword_shouldSetThatPassword(
    ) throws Exception {

        Password expectedPassword = dummy.getPassword();

        DatabaseAdapterConfig objectUnderTest =
                new DatabaseAdapterConfigImpl(
                        dummy.getUri(),
                        dummy.getUsername(),
                        expectedPassword
                );

        Password actualPassword =
                objectUnderTest.getPassword();

        assertThat(expectedPassword)
                .isSameAs(actualPassword);
    }

}