package com.precorconnect.spiffentitlementservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

public interface SpiffEntitlementsResponseFactory {

	SpiffEntitlementView construct(
			@NonNull SpiffEntitlement  spiffEntitlement
			);

}
