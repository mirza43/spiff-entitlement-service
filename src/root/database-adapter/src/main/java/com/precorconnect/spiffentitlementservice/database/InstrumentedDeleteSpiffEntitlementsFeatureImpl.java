package com.precorconnect.spiffentitlementservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;


public final class InstrumentedDeleteSpiffEntitlementsFeatureImpl implements DeleteSpiffEntitlementsFeature {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(InstrumentedDeleteSpiffEntitlementsFeatureImpl.class);
	
	private final DeleteSpiffEntitlementsFeature underlyingFeature;
	
	
	
	@Inject
	public InstrumentedDeleteSpiffEntitlementsFeatureImpl(@UnderlyingFeature final DeleteSpiffEntitlementsFeature underlyingFeature) {
		
		this.underlyingFeature =
                guardThat(
                        "underlyingFeature",
                        underlyingFeature
                )
                        .isNotNull()
                        .thenGetValue();
	}




	@Override
	public void deleteSpiffEntitlements(@NonNull List<SpiffEntitlementId> spiffEntitlementIds) {
		
		long startTime = System.currentTimeMillis();

					underlyingFeature
									.deleteSpiffEntitlements(
											spiffEntitlementIds
										);		

		long endTime = System.currentTimeMillis();

		LOGGER.debug(String
				.format("[In database deleteSpiffEntitlements call took %s millis.[start=%s, end=%s]",
						(endTime - startTime), new Date(startTime), new Date(
								endTime)));

		

	}

}
