package com.precorconnect.spiffentitlementservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;

@Singleton
public class DeleteSpiffEntitlementsFeatureImpl
		implements DeleteSpiffEntitlementsFeature{

	 private final SessionFactory sessionFactory;

   @Inject
   public DeleteSpiffEntitlementsFeatureImpl(
           @NonNull final SessionFactory sessionFactory
   ) {

   		this.sessionFactory =
   						guardThat(
   								"sessionFactory",
   								sessionFactory
   								)
   								.isNotNull()
   								.thenGetValue();

   }


	@Override
	public void deleteSpiffEntitlements(
			@NonNull List<SpiffEntitlementId> spiffEntitlementIds
			) {

		List<Long> spiffEntitlementIdsList = spiffEntitlementIds
															.stream()
															.map(id-> id.getValue())
															.collect(Collectors.toList());

		Session  session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        try {
        		Query query = session.createQuery("Delete from SpiffEntitlement se  where se.spiffEntitlementId in (:idList)");

        		query.setParameterList("idList", spiffEntitlementIdsList);

        		query.executeUpdate();

            	tx.commit();

        } catch(final Exception e) {

        	tx.rollback();
        	throw new RuntimeException("exception while committing transaction:", e);

        } finally {

        	if (session != null) {
                session.close();
            }

        }

	}

}
