package com.precorconnect.spiffentitlementservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;

public interface UpdateInvoiceUrlFeature {

	public void updateInvoiceUrl(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull InvoiceUrl invoiceUrl);

}
