package com.precorconnect.spiffentitlementservice.database;

public interface DatabaseAdapterConfigFactory {

	DatabaseAdapterConfig construct();
}
