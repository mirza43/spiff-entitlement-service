package com.precorconnect.spiffentitlementservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.precorconnect.AccountId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;

@Singleton
public class ListSpiffEntitlementFeatureImpl implements
		ListSpiffEntitlementFeature {

    private final SessionFactory sessionFactory;
    private final SpiffEntitlementsResponseFactory spiffEntitlementsResponseFactory;

    @Inject
    public ListSpiffEntitlementFeatureImpl(
            @NonNull final SessionFactory sessionFactory,
            @NonNull final SpiffEntitlementsResponseFactory spiffEntitlementsResponseFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                        sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffEntitlementsResponseFactory =
                guardThat(
                        "spiffEntitlementsResponseFactory",
                        spiffEntitlementsResponseFactory
                )
                        .isNotNull()
                        .thenGetValue();


    }

	@Override
	public Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(
			@NonNull AccountId accountId
			) {

		Session  session = sessionFactory.openSession();
        try {

            @SuppressWarnings("unchecked")
			List<SpiffEntitlement> spiffEntitlementDtos =
                    session
                    	.createQuery("from SpiffEntitlement where accountId= :accountId")
                    	.setParameter("accountId", accountId.getValue())
                    	.list();

            return spiffEntitlementDtos
                    .stream()
                    .map(spiffEntitlementsResponseFactory::construct)
                    .collect(Collectors.toList());

        } catch(final Exception e) {

        	throw new RuntimeException("exception while getting entitlement records:", e);

        } finally {

            if (session != null) {
                session.close();
            }

        }
	}

}
