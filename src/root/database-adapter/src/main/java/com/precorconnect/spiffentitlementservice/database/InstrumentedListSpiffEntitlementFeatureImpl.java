package com.precorconnect.spiffentitlementservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.Date;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.precorconnect.AccountId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;


public final class InstrumentedListSpiffEntitlementFeatureImpl implements ListSpiffEntitlementFeature {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(InstrumentedListSpiffEntitlementFeatureImpl.class);

	private final ListSpiffEntitlementFeature underlyingFeature;


	@Inject
	public InstrumentedListSpiffEntitlementFeatureImpl(@UnderlyingFeature final ListSpiffEntitlementFeature underlyingFeature) {
		
		this.underlyingFeature =
                guardThat(
                        "underlyingFeature",
                        underlyingFeature
                )
                        .isNotNull()
                        .thenGetValue();
		
	}

	@Override
	public Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(@NonNull AccountId accountId) {
		
		long startTime = System.currentTimeMillis();

		Collection<SpiffEntitlementView> result = underlyingFeature
													  .listEntitlementsWithPartnerId(
															  accountId															 
													);			

		long endTime = System.currentTimeMillis();

		LOGGER.debug(String
				.format("[In database listEntitlementsWithPartnerId call took %s millis.[start=%s, end=%s]",
						(endTime - startTime), new Date(startTime), new Date(
								endTime)));

		return result;
		
	}

}
