package com.precorconnect.spiffentitlementservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.FacilityName;
import com.precorconnect.spiffentitlementservice.objectmodel.FacilityNameImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InstallDate;
import com.precorconnect.spiffentitlementservice.objectmodel.InstallDateImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceNumber;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffAmount;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementIdImpl;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementViewImpl;

@Singleton
public class SpiffEntitlementsResponseFactoryImpl implements SpiffEntitlementsResponseFactory {

	@Override
	public SpiffEntitlementView construct(
									SpiffEntitlement spiffEntitlement){

		guardThat(
                "spiffEntitlement",
                spiffEntitlement
              )
               .isNotNull();

		SpiffEntitlementId spiffEntitlementId=
				new SpiffEntitlementIdImpl(
						spiffEntitlement
						.getSpiffEntitlementId()
						);

		AccountId accountId=
				new AccountIdImpl(
						spiffEntitlement
						.getAccountId()
						);

		PartnerSaleRegistrationId partnerSaleRegistrationId=
		  new PartnerSaleRegistrationIdImpl(
				  spiffEntitlement
				  .getPartnerSalRegistrationId()
				  );

		FacilityName facilityName=
				new FacilityNameImpl(
						spiffEntitlement
						.getFacilityName()
						);

		InvoiceNumber invoiceNumber=
				new InvoiceNumberImpl(
						spiffEntitlement
						.getInvoiceNumber()
				);

		InvoiceUrl invoiceUrl=
				new InvoiceUrlImpl(
						spiffEntitlement
						.getInvoiceUrl()
				);

		UserId partnerRepUserId=
				new UserIdImpl(
						spiffEntitlement
						.getPartnerRepId()
				);

		InstallDate installDate=
				new InstallDateImpl(
						spiffEntitlement
						.getInstallDate()
				);

		SpiffAmount spiffAmount=
				new SpiffAmountImpl(
						spiffEntitlement
						.getSpiffAmount()
					);

		return

				new SpiffEntitlementViewImpl(
								spiffEntitlementId,
								accountId,
								partnerSaleRegistrationId,
								facilityName,
								invoiceNumber,
								invoiceUrl,
								partnerRepUserId,
								installDate,
								spiffAmount
			     );
	}

}
