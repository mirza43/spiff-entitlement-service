package com.precorconnect.spiffentitlementservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;


@Singleton
public class SpiffEntitlementsRequestFactoryImpl implements SpiffEntitlementsRequestFactory {

	@Override
	public SpiffEntitlement construct(
			@NonNull SpiffEntitlementDto spiffEntitlementDto
			) {

        guardThat(
                  "spiffEntitlementDto",
                   spiffEntitlementDto
                )
                 .isNotNull();

		SpiffEntitlement entityObject = new SpiffEntitlement();

		entityObject.setAccountId(
				spiffEntitlementDto
				.getAccountId()
				.getValue()
				);

		entityObject.setPartnerSalRegistrationId(
				spiffEntitlementDto
				.getPartnerSaleRegistrationId()
				.getValue()
				);

		entityObject.setFacilityName(
				spiffEntitlementDto
				.getFacilityName()
				.getValue()
				);

		entityObject.setInvoiceNumber(
				spiffEntitlementDto
				.getInvoiceNumber()
				.getValue()
				);

		entityObject.setInvoiceUrl(
				spiffEntitlementDto
				.getInvoiceUrl()
				.getValue()
				);

		entityObject.setPartnerRepId(
				spiffEntitlementDto
				.getPartnerRepUserId()
				.getValue());

		entityObject.setInstallDate(
				spiffEntitlementDto
				.getInstallDate()
				.getValue()
				);

		entityObject.setSpiffAmount(
				spiffEntitlementDto
				.getSpiffAmount()
				.getValue());

		return entityObject;
	}

}
