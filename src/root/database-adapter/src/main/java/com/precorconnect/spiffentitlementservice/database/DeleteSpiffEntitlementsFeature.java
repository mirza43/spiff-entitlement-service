package com.precorconnect.spiffentitlementservice.database;

import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;

public interface DeleteSpiffEntitlementsFeature {

	void deleteSpiffEntitlements(
			@NonNull List<SpiffEntitlementId> spiffEntitlementIds
			);

}
