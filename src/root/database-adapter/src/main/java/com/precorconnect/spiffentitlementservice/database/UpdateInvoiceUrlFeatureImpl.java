package com.precorconnect.spiffentitlementservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.precorconnect.spiffentitlementservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffentitlementservice.objectmodel.PartnerSaleRegistrationId;

@Singleton
public class UpdateInvoiceUrlFeatureImpl implements UpdateInvoiceUrlFeature {

    private final SessionFactory sessionFactory;

    @Inject
    public UpdateInvoiceUrlFeatureImpl(
            @NonNull final SessionFactory sessionFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                        sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public void updateInvoiceUrl(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull InvoiceUrl invoiceUrl) {

		Session  session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        try {
            Query query= session
                    .createQuery("update SpiffEntitlement set invoiceUrl= :invoiceUrl"
                    					+" where partnerSaleRegistrationId= :partnerSaleRegistrationId"
                    			);

            query.setParameter("invoiceUrl", invoiceUrl.getValue());
            query.setParameter("partnerSaleRegistrationId", partnerSaleRegistrationId.getValue());
            query.executeUpdate();

            tx.commit();
        } catch(final Exception e) {

        	tx.rollback();
        	throw new RuntimeException("exception while committing transaction:", e);

        } finally {

            if (session != null) {
                session.close();
            }

        }

	}

}
