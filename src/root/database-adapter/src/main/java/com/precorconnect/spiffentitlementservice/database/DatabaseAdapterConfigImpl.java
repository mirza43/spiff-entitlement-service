package com.precorconnect.spiffentitlementservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.net.URI;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.Password;
import com.precorconnect.Username;

public class DatabaseAdapterConfigImpl
        implements DatabaseAdapterConfig {

    /*
    fields
     */
    private final URI uri;

    private final Username username;

    private final Password password;

    /*
    constructors
     */
    public DatabaseAdapterConfigImpl(
            @NonNull final URI uri,
            @NonNull final Username username,
            @NonNull final Password password
    ) {

    	this.uri =
                guardThat(
                        "uri",
                         uri
                )
                        .isNotNull()
                        .thenGetValue();

    	this.username =
                guardThat(
                        "username",
                         username
                )
                        .isNotNull()
                        .thenGetValue();

    	this.password =
                guardThat(
                        "password",
                        password
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
     */
    @Override
    public URI getUri() {
        return uri;
    }

    @Override
    public Username getUsername() {
        return username;
    }

    @Override
    public Password getPassword() {
        return password;
    }

}
