package com.precorconnect.spiffentitlementservice.database;

import java.util.Collection;
import java.util.List;

import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.spiffentitlementservice.objectmodel.SpiffEntitlementId;

public interface CreateSpiffEntitlementFeature {

	public Collection<SpiffEntitlementId> createSpiffEntitlements(
			List<SpiffEntitlementDto> spiffEntitlements
			);

}
