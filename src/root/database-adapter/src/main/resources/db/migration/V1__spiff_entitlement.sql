CREATE TABLE `spiffentitlements` (
  `spiffEntitlementId` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountId` bigint(20) NOT NULL,
  `partnerSaleRegistrationId` bigint(20) NOT NULL,
  `facilityName` varchar(100) NOT NULL,
  `invoiceNumber` varchar(100) NOT NULL,
  `invoiceUrl` varchar(3000) DEFAULT NULL,
  `partnerRepUserId` bigint(20) NOT NULL,
  `installDate` date NOT NULL,
  `spiffAmount` double NOT NULL,
  PRIMARY KEY (`spiffEntitlementId`)
);
