package com.precorconnect.spiffentitlementservice.identity;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerRepAccessContext;
import com.precorconnect.identityservice.sdk.IdentityServiceSdk;

@Singleton
public class GetPartnerRepAccessContextFeatureImpl implements
		GetPartnerRepAccessContextFeature {

	/*
    fields
     */
    private final IdentityServiceSdk identityServiceSdk;
    
    /*
    constructors
     */
    @Inject
    public GetPartnerRepAccessContextFeatureImpl(
            @NonNull final IdentityServiceSdk identityServiceSdk
    ) {

    	this.identityServiceSdk =
                guardThat(
                        "identityServiceSdk",
                        identityServiceSdk
                )
                        .isNotNull()
                        .thenGetValue();

    }

    
	@Override
	public PartnerRepAccessContext execute(
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException {
		
		return identityServiceSdk
				.getPartnerRepAccessContext(
											accessToken
										);
	}

}
