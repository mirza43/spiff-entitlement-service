package com.precorconnect.spiffentitlementservice.identity;

public interface IdentityServiceAdapterConfigFactory {

	IdentityServiceAdapterConfig construct();
}
