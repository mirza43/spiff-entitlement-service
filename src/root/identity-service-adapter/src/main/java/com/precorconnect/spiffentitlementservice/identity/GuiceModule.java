package com.precorconnect.spiffentitlementservice.identity;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.precorconnect.identityservice.sdk.IdentityServiceSdk;
import com.precorconnect.identityservice.sdk.IdentityServiceSdkConfig;
import com.precorconnect.identityservice.sdk.IdentityServiceSdkConfigImpl;
import com.precorconnect.identityservice.sdk.IdentityServiceSdkImpl;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final IdentityServiceAdapterConfig config;

    /*
    constructors
     */
    public GuiceModule(
            @NonNull final IdentityServiceAdapterConfig config
    ) {

    	this.config =
                guardThat(
                        "config",
                        config
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure() {

        bindFeatures();

    }

    private void bindFeatures() {

        bind(GetPartnerRepAccessContextFeature.class)
                .to(GetPartnerRepAccessContextFeatureImpl.class);

    }

    @Provides
    @Singleton
    IdentityServiceSdk identityServiceSdk(
            @NonNull final IdentityServiceSdkConfig identityServiceSdkConfig
    ) {

        return
                new IdentityServiceSdkImpl(identityServiceSdkConfig);

    }

    @Provides
    @Singleton
    IdentityServiceSdkConfig identityServiceSdkConfig() {

        return
                new IdentityServiceSdkConfigImpl(
                        config.getPrecorConnectApiBaseUrl()
                );

    }

}
