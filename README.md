## Description  
Precor Connect service responsible for spiff entitlements.

## Features

##### Create Spiff Entitlements
* [documentation](src/root/api/features/CreateSpiffEntitlements.feature)

##### List Spiff Entitlements With Id
* [documentation](src/root/api/features/ListEntitlementsWithPartnerId.feature)

##### Update Invoice Url
* [documentation](src/root/api/features/UpdateInvoiceUrl.feature)

##### Update Partner Rep
* [documentation](src/root/api/features/UpdatePartnerRep.feature)

## APIs
* [REST API](src/root/web-api/README.md)  

## SDKs  
* [SDK for Java](src/root/sdk/README.md)  
* [SDK for Javascript](https://bitbucket.org/precorconnect/spiff-entitlement-service-sdk-for-javascript)

## Configuration
Configuration is obtained through the environment variables listed below.
note: environment variables prefixed with `TEST_` are used only in integration tests.

|name|
|---|
|DATABASE_URI|
|DATABASE_USERNAME|
|DATABASE_PASSWORD|
|PRECOR_CONNECT_API_BASE_URL|
|TEST_IDENTITY_SERVICE_JWT_SIGNING_KEY|

## Develop:

#### Software
- git
- java 8 JDK
- maven
- docker toolbox

#### Scripts

set environment variables (perform prior to running or integration testing locally)
```PowerShell
 .\src\root\set-environment.ps1
```

compile & unit test
```PowerShell
mvn test -f .\src\root\pom.xml
```

compile & integration test
```PowerShell
mvn integration-test -f .\src\root\pom.xml
```

create docker image
```PowerShell
 mvn install -f .\src\root\pom.xml
```

run
```PowerShell
java -jar .\src\root\web-api\target\web-api.jar
```

run in docker container
```PowerShell
docker run -P spiff-entitlement-service
```

## spiff-entitlement-service sample REST request and response formats

1) createSpiffEntitlements

REQUEST:-->


               https://api-dev.precorconnect.com/spiff-entitlements/

HEADER:-->

    Content-Type: application/json

    Authorization:Bearer eyJhbGciOiJIUzI1NiJ9.eyJ0eXBlIjoicGFydG5lclJlcCIsImV4cCI6MTQ0OTkwNTg3NC44MjYwMDAwMDAsImF1ZCI6Imh0dHA6Ly9kZXYucHJlY29yY29ubmVjdC5jb20iLCJpc3MiOiJodHRwOi8vZGV2LnByZWNvcmNvbm5lY3QuY29tIiwiZ2l2ZW5fbmFtZSI6InZlbmthdGFyYW1hbmEiLCJmYW1pbHlfbmFtZSI6IlAiLCJzdWIiOiIwMHU1aDhyaGJ0elBFdENIMjBoNyIsImFjY291bnRfaWQiOiIwMDFLMDAwMDAxSDJLbTJJQUYiLCJzYXBfdmVuZG9yX251bWJlciI6IjAwMDAwMDAwMDAifQ.l6wGCuHFkh7WLQXS94pAj6DTJ_CJizwMgKXDHuSfx7I


  METHOD:-->

                   POST

  REQUESTBODY:



     [
           {
             "accountId":"123456789012345678",
             "partnerSaleRegistrationId":14,
             "facilityName":"testfacility",
             "invoiceNumber":"12345678",
             "invoiceUrl":"xyz",
             "partnerRepUserId":"00u5ffbkz0Au9HKQg0h7",
              "installDate":"12/12/2015",
              "spiffAmount":140.0
               }
      ]


RESPONSE:


          Status Code: 200 OK


    [
        3
    ]


-----------------------------------------------------------------------------------------------

2) ListEntitlementsWithPartnerId

HEADER:-->

    Content-Type: application/json

    Authorization:Bearer eyJhbGciOiJIUzI1NiJ9.eyJ0eXBlIjoicGFydG5lclJlcCIsImV4cCI6MTQ0OTkwNTg3NC44MjYwMDAwMDAsImF1ZCI6Imh0dHA6Ly9kZXYucHJlY29yY29ubmVjdC5jb20iLCJpc3MiOiJodHRwOi8vZGV2LnByZWNvcmNvbm5lY3QuY29tIiwiZ2l2ZW5fbmFtZSI6InZlbmthdGFyYW1hbmEiLCJmYW1pbHlfbmFtZSI6IlAiLCJzdWIiOiIwMHU1aDhyaGJ0elBFdENIMjBoNyIsImFjY291bnRfaWQiOiIwMDFLMDAwMDAxSDJLbTJJQUYiLCJzYXBfdmVuZG9yX251bWJlciI6IjAwMDAwMDAwMDAifQ.l6wGCuHFkh7WLQXS94pAj6DTJ_CJizwMgKXDHuSfx7I


REQUEST:-->
             https://api-dev.precorconnect.com/spiff-entitlements/accountId

example:

          https://api-dev.precorconnect.com/spiff-entitlements/001K000001H2Km2IAF


METHOD:-->


             GET

RESPONSE:


             Status Code: 200 OK



     [
           {
              "spiffEntitlementId": 1,
              "accountId": "001K000001H2Km2IAF",
              "partnerSaleRegistrationId": 12,
              "facilityName": "precor",
              "invoiceNumber": "1113112",
              "invoiceUrl": "abc",
              "partnerRepUserId": "00u5ffbkz0Au9HKQg0h7",
              "installDate": "11/21/1999",
              "spiffAmount": 100

           }
    ]


-----------------------------------------------------------------------------


3) updateInvoiceUrl


HEADER:-->

    Content-Type: application/json

    Authorization:Bearer eyJhbGciOiJIUzI1NiJ9.eyJ0eXBlIjoicGFydG5lclJlcCIsImV4cCI6MTQ0OTkwNTg3NC44MjYwMDAwMDAsImF1ZCI6Imh0dHA6Ly9kZXYucHJlY29yY29ubmVjdC5jb20iLCJpc3MiOiJodHRwOi8vZGV2LnByZWNvcmNvbm5lY3QuY29tIiwiZ2l2ZW5fbmFtZSI6InZlbmthdGFyYW1hbmEiLCJmYW1pbHlfbmFtZSI6IlAiLCJzdWIiOiIwMHU1aDhyaGJ0elBFdENIMjBoNyIsImFjY291bnRfaWQiOiIwMDFLMDAwMDAxSDJLbTJJQUYiLCJzYXBfdmVuZG9yX251bWJlciI6IjAwMDAwMDAwMDAifQ.l6wGCuHFkh7WLQXS94pAj6DTJ_CJizwMgKXDHuSfx7I


REQUEST:-->


                    https://api-dev.precorconnect.com/spiff-entitlements/{partnerSaleRegistrationId}/invoiceurl


example:


      https://api-dev.precorconnect.com/spiff-entitlements/14/invoiceurl

REQUESTBODY:-->


                    test.com

METHOD:-->


                       POST

RESPONSE:


                  Status Code: 200 OK


-----------------------------------------------------------------------------



4) updatePartnerRep

HEADER:-->

    Content-Type: application/json

    Authorization:Bearer eyJhbGciOiJIUzI1NiJ9.eyJ0eXBlIjoicGFydG5lclJlcCIsImV4cCI6MTQ0OTkwNTg3NC44MjYwMDAwMDAsImF1ZCI6Imh0dHA6Ly9kZXYucHJlY29yY29ubmVjdC5jb20iLCJpc3MiOiJodHRwOi8vZGV2LnByZWNvcmNvbm5lY3QuY29tIiwiZ2l2ZW5fbmFtZSI6InZlbmthdGFyYW1hbmEiLCJmYW1pbHlfbmFtZSI6IlAiLCJzdWIiOiIwMHU1aDhyaGJ0elBFdENIMjBoNyIsImFjY291bnRfaWQiOiIwMDFLMDAwMDAxSDJLbTJJQUYiLCJzYXBfdmVuZG9yX251bWJlciI6IjAwMDAwMDAwMDAifQ.l6wGCuHFkh7WLQXS94pAj6DTJ_CJizwMgKXDHuSfx7I


REQUEST:-->


              https://api-dev.precorconnect.com/spiff-entitlements/{partnerSaleRegistrationId}/partnerrepuserid/{partnerRepUserId}



example:


                   https://api-dev.precorconnect.com/spiff-      entitlements/14/partnerrepuserid/00u5ffbkz0Au9HKQg0h5



METHOD:-->


                  POST

RESPONSE:


               Status Code: 200 OK


-----------------------------------------------------------------------------

5)

HEADER:-->

    Content-Type: application/json

    Authorization:Bearer eyJhbGciOiJIUzI1NiJ9.eyJ0eXBlIjoicGFydG5lclJlcCIsImV4cCI6MTQ0OTkwNTg3NC44MjYwMDAwMDAsImF1ZCI6Imh0dHA6Ly9kZXYucHJlY29yY29ubmVjdC5jb20iLCJpc3MiOiJodHRwOi8vZGV2LnByZWNvcmNvbm5lY3QuY29tIiwiZ2l2ZW5fbmFtZSI6InZlbmthdGFyYW1hbmEiLCJmYW1pbHlfbmFtZSI6IlAiLCJzdWIiOiIwMHU1aDhyaGJ0elBFdENIMjBoNyIsImFjY291bnRfaWQiOiIwMDFLMDAwMDAxSDJLbTJJQUYiLCJzYXBfdmVuZG9yX251bWJlciI6IjAwMDAwMDAwMDAifQ.l6wGCuHFkh7WLQXS94pAj6DTJ_CJizwMgKXDHuSfx7I


REQUEST:-->


     https://api-dev.precorconnect.com/spiff-entitlements/spiffentitlementids/



REQUESTBODY:-->


                  [3]

METHOD:-->


            POST

RESPONSE:


            Status Code: 200 OK


-----------------------------------------------------------------------------